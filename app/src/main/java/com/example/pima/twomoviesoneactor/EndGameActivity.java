package com.example.pima.twomoviesoneactor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EndGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);

        Intent intent = getIntent();
        int score = intent.getIntExtra("score",0);
        String message = "Vous avez marqué " + String.valueOf(score) + " pts !";

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainMenu.class);
        finish();
        startActivity(intent);
    }

    public void retry(View view){
        Intent intent = new Intent(this, GameActivity.class);
        finish();
        startActivity(intent);
    }

    public void mainmenu(View view){
        Intent intent = new Intent(this, MainMenu.class);
        finish();
        startActivity(intent);
    }

    public void quit(View view){
        finish();
        System.exit(0);
    }
}
