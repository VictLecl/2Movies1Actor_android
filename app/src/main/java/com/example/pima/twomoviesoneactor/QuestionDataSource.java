package com.example.pima.twomoviesoneactor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 17/11/17.
 */

public class QuestionDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_FILM_1,
            MySQLiteHelper.COLUMN_FILM_2,
            MySQLiteHelper.COLUMN_ANSWER1,
            MySQLiteHelper.COLUMN_ANSWER2,
            MySQLiteHelper.COLUMN_ANSWER3,
            MySQLiteHelper.COLUMN_ANSWER4,};

    public QuestionDataSource(Context context){
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Question createQuestion(String film1, String film2, String answer1,String answer2,String answer3,String answer4) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_FILM_1, film1);
        values.put(MySQLiteHelper.COLUMN_FILM_2, film2);
        values.put(MySQLiteHelper.COLUMN_ANSWER1, answer1);
        values.put(MySQLiteHelper.COLUMN_ANSWER2, answer2);
        values.put(MySQLiteHelper.COLUMN_ANSWER3, answer3);
        values.put(MySQLiteHelper.COLUMN_ANSWER4, answer4);
        long insertId = database.insert(MySQLiteHelper.TABLE_QUESTIONS, null,
                values);
        /*Cursor cursor = database.query(MySQLiteHelper.TABLE_QUESTIONS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();*/
        Question newQuestion = new Question();/*cursorToQuestion(cursor);*/
        //cursor.close();
        return newQuestion;
    }

    public void deleteQuestion(Question question) {
        long id = question.getId();
        System.out.println("com.example.pima.twomoviesoneactor.Question deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_QUESTIONS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Question> getAllQuestions() {
        List<Question> questions = new ArrayList<Question>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_QUESTIONS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Question question = cursorToQuestion(cursor);
            questions.add(question);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return questions;
    }

    private Question cursorToQuestion(Cursor cursor) {
        Question question = new Question();
        question.setId(cursor.getLong(0));
        question.setQuestion(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));
        return question;
    }
}