package com.example.pima.twomoviesoneactor;

/**
 * Created by victor on 17/11/17.
 */

public class Question {
    private long id;
    private String Film1;
    private String Film2;
    private String Answer1;
    private String Answer2;
    private String Answer3;
    private String Answer4;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFilm1() {
        return Film1;
    }

    public void setFilm1(String film1) {
        Film1 = film1;
    }

    public String getFilm2() {
        return Film2;
    }

    public void setFilm2(String film2) {
        Film2 = film2;
    }

    public String getAnswer1() {
        return Answer1;
    }

    public void setAnswer1(String answer1) {
        Answer1 = answer1;
    }

    public String getAnswer2() {
        return Answer2;
    }

    public void setAnswer2(String answer2) {
        Answer2 = answer2;
    }

    public String getAnswer3() {
        return Answer3;
    }

    public void setAnswer3(String answer3) {
        Answer3 = answer3;
    }

    public String getAnswer4() {
        return Answer4;
    }

    public void setAnswer4(String answer4) {
        Answer4 = answer4;
    }

    public void setQuestion(String film1, String film2, String answer1, String answer2, String answer3, String answer4) {
        Film1 = film1;
        Film2 = film2;
        Answer1 = answer1;
        Answer2 = answer2;
        Answer3 = answer3;
        Answer4 = answer4;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return Answer1;
    }
}
