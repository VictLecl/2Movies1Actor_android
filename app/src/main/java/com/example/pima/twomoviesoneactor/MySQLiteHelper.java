package com.example.pima.twomoviesoneactor;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by victor on 17/11/17.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {
    private Context context;

    public static final String TABLE_QUESTIONS = "questions";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FILM_1 = "film1";
    public static final String COLUMN_FILM_2 = "film2";
    public static final String COLUMN_ANSWER1 = "answer1";
    public static final String COLUMN_ANSWER2 = "answer2";
    public static final String COLUMN_ANSWER3 = "answer3";
    public static final String COLUMN_ANSWER4 = "answer4";

    private static final String DATABASE_NAME = "questions.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE =
            "create table " + TABLE_QUESTIONS + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FILM_1 + " text, "
            + COLUMN_FILM_2 + " text, "
            + COLUMN_ANSWER1 + " text, "
            + COLUMN_ANSWER2 + " text, "
            + COLUMN_ANSWER3 + " text, "
            + COLUMN_ANSWER4 + " text"
            + ");";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

        //fillDataXML("database",);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
            database.execSQL(DATABASE_CREATE);
            database.execSQL("INSERT INTO Questions (film1,film2,answer1,answer2,answer3,answer4) " +
                    "VALUES ('film1','film2','reponse','fausereponse','fausereponse','fausereponse')");
            fillDataXML("database2",database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        onCreate(db);
    }

    private void fillDataXML(String xmlName, SQLiteDatabase database) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            InputStream is = context.getResources().openRawResource(R.raw.database2);
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            xpp.setInput(r);

            Question question = new Question();
            String test = null;

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    Log.d("fillDataXml","Start document");
                } else if (eventType == XmlPullParser.END_DOCUMENT) {
                    Log.d("fillDataXml","End document");
                } else if (eventType == XmlPullParser.START_TAG) {
                    Log.d("fillDataXml","Start tag " + xpp.getName());
                    test= xpp.getName();
                } else if (eventType == XmlPullParser.END_TAG) {
                    Log.d("fillDataXml","End tag " + xpp.getName());
                } else if (eventType == XmlPullParser.TEXT) {
                    Log.d("fillDataXml","Text " + xpp.getText());
                    switch (test) {
                        case "Film1" :
                            question.setFilm1(xpp.getText());
                            test = "";
                            break;
                        case "Film2" :
                            question.setFilm2(xpp.getText());
                            test = "";
                            break;
                        case "Answer1" :
                            question.setAnswer1(xpp.getText());
                            test = "";
                            break;
                        case "Answer2" :
                            question.setAnswer2(xpp.getText());
                            test = "";
                            break;
                        case "Answer3" :
                            question.setAnswer3(xpp.getText());
                            test = "";
                            break;
                        case "Answer4" :
                            question.setAnswer4(xpp.getText());
                            test = "add";
                            break;

                        case "add":
                            ContentValues values = new ContentValues();
                            values.put(COLUMN_FILM_1, question.getFilm1());
                            values.put(COLUMN_FILM_2, question.getFilm2());
                            values.put(COLUMN_ANSWER1, question.getAnswer1());
                            values.put(COLUMN_ANSWER2, question.getAnswer2());
                            values.put(COLUMN_ANSWER3, question.getAnswer3());
                            values.put(COLUMN_ANSWER4, question.getAnswer4());
                            long insertId = database.insert(TABLE_QUESTIONS, null,
                                    values);
                            Log.d("fillDataXMl","insertion resultat"+String.valueOf(insertId));
                            test = "";
                            break;
                    }
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.e("fillDataXml", "erreur incoonue");
        }
    }
}
