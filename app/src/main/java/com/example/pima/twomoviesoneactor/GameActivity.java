package com.example.pima.twomoviesoneactor;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    private QuestionDataSource datasource;
    private List<Question> listQuestions;
    private Question currentQuestion;

    private Button repA,repB, repC, repD;
    private TextView questionText, scoreText, timerText;

    private GameTimer gameTimer;
    private int score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init var buttons
        repA = findViewById(R.id.buttonA);
        repB = findViewById(R.id.buttonB);
        repC = findViewById(R.id.buttonC);
        repD = findViewById(R.id.buttonD);

        //Init var Text
        questionText = findViewById(R.id.questionView);
        scoreText = findViewById(R.id.scoreView);
        timerText = findViewById(R.id.timerView);

        // Ouverture SQLite
        datasource = new QuestionDataSource(this);
        datasource.open();

        // Récup. de toutes les questions de la bdd
        listQuestions = datasource.getAllQuestions();

        // Affichage de la premiere question
        newQuestion();

        // Init score
        score=0;
        scoreText.setText(String.valueOf(score)+" pts");

        // Lancement Timer
        gameTimer = new GameTimer(60000,1000);
        gameTimer.start();
    }

    public void checkResponse(View v){
        Button button = findViewById(v.getId());
        Log.i("Test Reponse",button.getText()+" et "+currentQuestion.getAnswer1());
        if(button.getText()==currentQuestion.getAnswer1())
            addScore(5);
        newQuestion();
    }

    /**
     * Ajoute des points au score de l'utilisateur,
     * le score ne peut pas etre negatif
     * @param pts le nombre de point à ajouter (peut etre négatif)
     */
    public void addScore(int pts){
        Log.i("score","Ajout de 5 pts de scores");
        score += pts;
        Log.i("score","score vaut "+String.valueOf(score));
        if(score < 0)
            score = 0;
        scoreText.setText(String.valueOf(score)+" pts");
    }

    /**
     * Affichage d'une question au hasard, parmis la liste des questions,
     * et actualisation des elements sur l'ecran
     */
    public void newQuestion(){
        // Choix d'une question au hasard
        Random r = new Random();
        int min = 0;
        int max = listQuestions.size()-1;
        int i = r.nextInt((max-min)+1)+min;
        currentQuestion = listQuestions.get(i);

        // Affichage d'une question
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Quel acteur a joué dans ");
        stringBuilder.append(currentQuestion.getFilm1());
        stringBuilder.append(" et ");
        stringBuilder.append(currentQuestion.getFilm2());
        stringBuilder.append(" ?");

        questionText.setText(stringBuilder.toString());

        // mélange
        ArrayList<String> answerList = new ArrayList<String>();
        answerList.add(currentQuestion.getAnswer1());
        answerList.add(currentQuestion.getAnswer2());
        answerList.add(currentQuestion.getAnswer3());
        answerList.add(currentQuestion.getAnswer4());
        Collections.shuffle(answerList);

        // Affectation des boutons
        repA.setText(answerList.get(0));
        repB.setText(answerList.get(1));
        repC.setText(answerList.get(2));
        repD.setText(answerList.get(3));
    }

    public void endGame(){
        Intent intent = new Intent(this, EndGameActivity.class);
        intent.putExtra("score", score);
        finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("2 Movies A Actor");
        builder.setMessage("Voulez vous vraiment abandonner ? ");
        builder.setPositiveButton("Je suis faible, j'abandonne ...", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //saveResult();
                gameTimer.cancel();
                finish();
                GameActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Bien sur que Non !", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //GameActivity.super.onBackPressed();
            }
        });
        builder.show();
    }

    /**
     * Classe de timer
     */
    public class GameTimer extends CountDownTimer {

        private ProgressBar progressBar;
        private TextView timerView;

        public GameTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            progressBar = findViewById(R.id.progressBar);
            timerView = findViewById(R.id.timerView);
        }

        /**
         * Action à chaque periode du timer
         * Actualisation des widgets
         * @param l la valeur courante du timer
         */
        @Override
        public void onTick(long l) {
            int progress = (int) (l/1000);
            progressBar.setProgress(progress);
            timerView.setText(String.valueOf(progress)+"s");
        }

        /**
         * Action de fin du timer
         * Redirection vers la page de fin
         */
        @Override
        public void onFinish() {
            Log.d("timer","fin du timer");
            endGame();
        }
    }
}
